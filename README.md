# The Thing

This is a practicing example for the famous AP4 team.


    git clone git@bitbucket.org:n0ckenfell/ap4-practice.git
    cd ap4-practice
    bundle
    rspec -fd

should output

    Rspec works
      tests false is FALSE
      nil is FALSE
      everything else, even 0 is true

    TheThing
      should have a name
      should have an id
      should have an unique id
      should not change the id once it was created

    Finished in 0.00316 seconds (files took 0.08812 seconds to load)
    7 examples, 0 failures

