describe "Rspec works" do

  it "tests false is FALSE" do
     expect(false).to be_falsy
  end

  it "nil is FALSE" do
    expect(nil).to be_falsy
  end

  it "everything else, even 0 is true" do
    expect(0).to be_truthy
    expect("").to be_truthy
  end

end
