describe TheThing do

  before  do
    @thing = TheThing.new("One")
  end

  it "should have a name" do
    expect( @thing.name ).to eq("One")
  end

  it "should have an id" do
    expect( @thing.id ).not_to be_nil
  end

  it "should have an unique id" do
    other_thing = TheThing.new("Other")
    expect( @thing.id).not_to be(other_thing.id)
  end

  it "should not change the id once it was created" do
    expect( @thing.id ).to be(@thing.id)
  end

end

